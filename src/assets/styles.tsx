import styled from 'styled-components'

export const colors =  {
  primary: '#116193',
  secondary: '#00e7e3',
  support: '#ebebeb',
  supportType: '#555555',
}

export const size = {
  extraSmall: '(max-width: 575px)',
  small: '(min-width: 576px) and (max-width: 767px)',
  medium: '(min-width: 768px) and (max-width: 991px)',
  large: '(min-width: 992px) and (max-width: 1199px)',
  extraLarge: '(min-width: 1200px)',
}


export interface Props {
  toggleVisibleXS?: boolean
}

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const RowContainer = styled(Container)`
  flex-direction: row;
`;

export const CenterContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const AppContainer = styled(Container)`
  width: 100%;
  height: 100vh;
`;

export const AppHeader = styled.header`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 15px 0;
  min-height: 80px;
  background-color: ${colors.primary};
  h1 {
    font-weight: normal;
    color: ${colors.secondary};
    font-size: 2.5rem;
    margin: 0;
    text-transform: capitalize;
  }
`;

export const AppContent = styled(Container)`
  flex: 1;
  position: relative;
`;
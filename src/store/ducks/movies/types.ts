
// Action Types

export enum MoviesTypes {
    LOAD_REQUEST = '@movies/LOAD_REQUEST',
    LOAD_FAILURE = '@movies/LOAD_FAILURE',
    LOAD_SUCCES = '@movies/LOAD_SUCCES',
}

// Data Types

export interface Movie {
    id: number
    popularity: number
    vote_count: number
    video: boolean
    poster_path:  string
    adult: boolean
    backdrop_path:  string
    original_language:  string
    original_title:  string
    genre_ids: number[]
    title:  string
    vote_average: number
    overview:  string
    release_date:  string
    genres?: genre[]
}

export interface genre {
    id: number
    name: string
}

// State Types

export interface MoviesState {
    readonly data: Movie[]
    readonly loading: boolean
    readonly error: boolean
    lastReasearch: string,
}

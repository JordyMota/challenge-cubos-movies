import { call, put } from 'redux-saga/effects';

import api, { defaultParams, singleRoute } from '../../../services/api';
import { loadSucces, loadFailure } from './actions';

export function* load({payload}:any) {
    try {
        const response = yield call(api.get,singleRoute+payload+defaultParams);
        const videoResponse = yield call(api.get,singleRoute+payload+'/videos'+defaultParams);
        response.data.videos = videoResponse.data.results;
        yield put(loadSucces(response.data))
    } catch (err) {
        yield put(loadFailure())
    }
}
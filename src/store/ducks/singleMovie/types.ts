
// Action Types

export enum SingleMoviesTypes {
    LOAD_REQUEST = '@singleMovies/LOAD_REQUEST',
    LOAD_FAILURE = '@singleMovies/LOAD_FAILURE',
    LOAD_SUCCES = '@singleMovies/LOAD_SUCCES',
}

// Data Types

export interface genre {
    id: number
    name: string
}


export interface SingleMovie {
    adult: false
    backdrop_path: string | null
    belongs_to_collection: any
    budget: number
    genres: genre[]
    homepage: string | null
    id: number
    imdb_id: string | null
    original_language: string
    original_title: string
    overview: string | null
    popularity: number
    poster_path: string | null
    production_companies: {
        id: number
        logo_path: string | null
        name: string
        origin_country: string
    }[]
    production_countries: {
        iso_3166_1: string
        name: string
    }[]
    release_date: string
    revenue: number
    runtime: number | null
    spoken_languages: {
        iso_639_1: string
        name: string
    }[]
    status: string
    tagline: string | null
    title: string
    video: boolean
    vote_average: number
    vote_count: number
    videos: Videos[]
} 

export interface Videos {
    id: string
    iso_639_1: string
    iso_3166_1: string
    key: string
    name: string
    site: string
    size: number
    type: string
}

// State Types

export interface SingleMoviesState {
    readonly data: SingleMovie | null
    readonly loading: boolean
    readonly error: boolean
}
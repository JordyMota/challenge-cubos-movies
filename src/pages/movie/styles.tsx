import styled from 'styled-components';
import * as global from '../../assets/styles';

export const MovieItem = styled(global.Container)`
    width: 100vw;
    height: auto;
    background-color: ${global.colors.support};
    position: relative;
    margin-top: 14vh;
    @media ${ global.size.extraSmall } {
        margin-top: 8vh;
    }
`;

export const MovieTitle = styled(global.RowContainer)`
    width: 100%;
    background-color: #e1e1e1;
    max-height: 65px;
    min-height: 65px;
    align-items: flex-end;
    justify-content: space-between;
    padding: 0 16px 14px 32px;
    h2 {
        text-transform: capitalize;
        font-weight: normal;
        color: ${global.colors.primary};
        font-size: 1.5rem;
        margin: 0;
        transform: scaleY(1.1);
    }
    span {
        font-size: 1.1rem;
        color: ${global.colors.supportType};
    }
    @media ${ global.size.extraSmall } {
        max-height: 40px;
        min-height: 40px;
        padding-left: 30px;
        padding-bottom: 10px;
        h2 {
            font-size: 1rem;
        }
        span: {
            font-size: .78rem;
        }
    }
`;

export const MovieContent = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 100vw;
    justify-content: space-between;
    padding-left: 16px;
    div {
        width: 100%;
        padding: 16px;
        max-width: calc(100vw - 34vw);
        position: relative;
    }
    @media ${ global.size.extraSmall } {
        padding-left: 0;
        div {
            max-width: 100vw;
        }
    }
`;

export const MoviePoster = styled.img`
    width: 30vw;
    max-width: 30vw;
    height: auto;
    max-height: 45.071vw;
    @media ${ global.size.extraSmall } {
        width: 100vw;
        max-width: 100vw;
        max-height: unset;
        order: -1;
    }
`;

export const InfoLabel = styled.div`
    width: 100%;
    padding-bottom: 8px;
    border-bottom: 1px solid ${global.colors.secondary};
    margin-top: 30px;
    margin-bottom: 13px;
    h3 {
        color: ${global.colors.primary};
        font-size: 1.3rem;
        font-weight: normal;
        margin: 0;
    }
`;

export const Descript = styled.div`
    width: 100%;
    white-space: normal;
    color: ${global.colors.supportType};
    font-size: .9rem;
    @media ${ global.size.extraSmall } {
        font-size: .73rem;
    }
`;

export const Infos = styled.div`
    max-width: 100%;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-between;
    div {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        width: auto;
        font-weight: bold;
        p {
            display: flex;
            margin: 0;
            font-size: .93rem;
            color: ${global.colors.primary};
            transform: scaleY(1.1);
        }
        span {
            display: flex;
            font-size: .85rem;
            margin-top: 3px;
            color: ${global.colors.supportType};
            transform: scaleY(1.1);
        }
    }
    @media ${ global.size.extraSmall } {
        div {
            p {
                font-size: .85rem;
            }
            span {
                font-size: .75rem;
            }
        }
    }
`;

export const GenreList = styled(global.RowContainer)`
    margin: 3vh 0 0;
    flex-wrap: nowrap;
    overflow-x: auto;
    max-width: 90%;
    span {
        padding: 6px 12px;
        background-color: #fff;
        color: ${global.colors.primary};
        border: 1px solid ${global.colors.primary};
        border-radius: 20px;
        margin-right: 2%;
        font-size: .75rem;
    }    
`;

export const MovieRate = styled(global.CenterContainer)`
    width: 130px !important;
    height: 130px;
    max-width: 130px !important;
    background-color: ${global.colors.primary};
    border-radius: 50%;
    align-self: flex-end;
    overflow: hidden;
    margin-right: 16px;
    margin-bottom: 16px;
    float: right;
    z-index: 18;
    div {
        width: 123px !important;
        min-width: 123px !important;
        height: 123px !important;
        border-radius: 50%;
        border: 5px solid ${global.colors.secondary};
        display: flex;
        justify-content: center;
        align-items: center;
        span {
            font-size: 1.8rem;
            color: ${global.colors.secondary};
        }
    }
`;

export const MainContainer = styled.div`
    width: 100vw;
    height: auto;
    position: relative;
`;

export const VideoFrame = styled.div`
    width: 100vw;
    height: auto;
    height: 600px;
    position: relative;
    margin-bottom: 60px;
    margin-top: 40px;
    iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }
    @media ${ global.size.extraSmall } {
        height: 300px;
    }
`;
import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { Descript, InfoLabel, MoviePoster, MovieContent, MovieTitle, MovieItem, Infos, GenreList, MovieRate, MainContainer, VideoFrame } from './styles';
import { AplicationState } from '../../store';
import { SingleMovie } from '../../store/ducks/singleMovie/types';
import * as MovieActions from '../../store/ducks/singleMovie/actions';
import { imgRoute } from '../../services/api';

const API_KEY = 'AIzaSyAw8lpQvFRvdzgpTO1eRxxX7FjcOcCGNKI';

interface DispatchProps {
    loadRequest(id: string): void
}

interface StateProps {
    singleMovie?: SingleMovie
}

interface OwnProps extends RouteComponentProps<{id: string}> {
    
}

type Props = DispatchProps & OwnProps & StateProps;

class Movie extends Component<Props> {

    componentDidMount() {
        const { loadRequest, match } = this.props;
        loadRequest(match.params.id.toString());
    }

    render() {
        const { singleMovie } = this.props
        return (
            !singleMovie ?
            (<p>Erro ao encontrar o filme</p>) :
            (
                <MainContainer>
                    <MovieItem
                        className="limit-charset"
                    >
                        <MovieTitle
                            className="limit-charset"
                        >
                            <h2
                                className="limit-charset font-titillium"
                            >
                                { singleMovie.title }
                            </h2>
                            <span>
                                { new Date(singleMovie.release_date).toLocaleDateString() }
                            </span>
                        </MovieTitle>
                        <MovieContent>
                            <div>
                                <InfoLabel>
                                    <h3
                                        className="font-titillium"
                                    >
                                        Sinopse
                                    </h3>
                                </InfoLabel>
                                <Descript>
                                    {singleMovie.overview}
                                </Descript>
                                <InfoLabel>
                                    <h3
                                        className="font-titillium"
                                    >
                                        Informações
                                    </h3>
                                </InfoLabel>
                                <Infos>
                                    <div>
                                        <p>
                                            Situação
                                        </p>
                                        <span>
                                            {singleMovie.status}
                                        </span>
                                    </div>
                                    <div>
                                        <p>
                                            Idioma
                                        </p>
                                        <span>
                                            {singleMovie.original_language}
                                        </span>
                                    </div>

                                    {(singleMovie.runtime && singleMovie.runtime) && (<div>
                                        <p>
                                            Duração
                                        </p>
                                        <span>
                                            {parseInt((singleMovie.runtime/60).toString())}h {singleMovie.runtime%60}m
                                        </span>
                                    </div>)}
                                    <div>
                                        <p>
                                            Orçamento
                                        </p>
                                        <span>
                                            R$ {singleMovie.budget}
                                        </span>
                                    </div>
                                    <div>
                                        <p>
                                            Receita
                                        </p>
                                        <span>
                                            R$ {singleMovie.revenue}
                                        </span>
                                    </div>
                                    <div>
                                        <p>
                                            Lucro
                                        </p>
                                        <span>
                                            R$ {singleMovie.revenue - singleMovie.budget}
                                        </span>
                                    </div>
                                </Infos>
                                {singleMovie.genres && <GenreList>
                                        { 
                                            singleMovie.genres.map(item => (<span key={item.id} >{item.name}</span>))
                                        }
                                </GenreList>}
                                <MovieRate>
                                    <div>
                                        <span
                                            className="font-titillium"
                                        >
                                            {singleMovie.vote_average*10}%
                                        </span>
                                    </div>
                                </MovieRate>
                            </div>
                            <MoviePoster
                                src={ imgRoute + singleMovie.poster_path }
                            />
                        </MovieContent>
                    </MovieItem>
                    {(singleMovie.videos && singleMovie.videos[0]) && <VideoFrame>
                        <iframe
                            src={`https://www.youtube.com/embed/${singleMovie.videos[0].key}`}
                            frameBorder="0"
                        />
                    </VideoFrame>}
                </MainContainer>
            )
        );
    }
}

const mapStateToProps = ({ singleMovie }: AplicationState) => {
    let data;
    if(singleMovie.data)
        data = singleMovie.data;
    return{
        singleMovie: data
    };
};

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(MovieActions, dispatch);

export default connect(mapStateToProps,mapDispatchToProps)(Movie);

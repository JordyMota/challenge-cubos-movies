import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { SearchbarContainer } from './styles';
import * as MovieActions from '../../store/ducks/movies/actions';

interface DispatchProps {
    loadRequest(text: string): void
}

type Props = DispatchProps;

class Searchbar extends Component<Props> {
    state = {
        text: '',
    }

    
    componentDidMount() {}
    
    handleUserType = ({target}:any)=> {
        this.setState({
            text: target.value,
        });
    }
    
    handleUserSearch = (event:any)=> {
        const { loadRequest } = this.props;
        event.preventDefault();
        const searchString = this.state.text.trim().toLowerCase().replace(/ /g,'+');
        if(searchString.length)
            loadRequest(searchString);
    }

    render() {
        return (
        <SearchbarContainer
            onSubmit={this.handleUserSearch}
        >
            <input
                type="text"
                placeholder="Busque um filme por nome ou gênero..."
                onChange={this.handleUserType}
            />
        </SearchbarContainer>
        );
    }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(MovieActions, dispatch);

export default connect(null,mapDispatchToProps)(Searchbar)

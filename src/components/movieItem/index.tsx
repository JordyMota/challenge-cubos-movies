import React from 'react';
import { Link } from 'react-router-dom';

import { MovieContainer, MoviePoster, MovieTitle, MovieContent, MovieRate, ReleaseDate, MovieDescript, GenreList } from './styles';
import { Movie } from '../../store/ducks/movies/types';
import { imgRoute } from '../../services/api';

interface Props {
    movie: Movie
}

const MovieItem = ({movie}:Props) => (
    <MovieContainer>
        <Link
            to={'/movie/'+movie.id }
        >
            <MoviePoster src={imgRoute+movie.poster_path}/>
            <MovieRate
                toggleVisibleXS={true}
            >
                <div>
                    <span
                        className="font-titillium"
                    >
                        {movie.vote_average*10}%
                    </span>
                </div>
            </MovieRate>
            <MovieContent
                className="limit-charset"
            >
                <MovieRate>
                    <div>
                        <span
                            className="font-titillium"
                        >
                            {movie.vote_average*10}%
                        </span>
                    </div>
                </MovieRate>
                <MovieTitle
                    className="limit-charset"
                >
                    <h2
                        className="limit-charset font-titillium"
                    >
                        {movie.title}
                    </h2>
                </MovieTitle>
                <ReleaseDate>
                    { new Date(movie.release_date).toLocaleDateString() }
                </ReleaseDate>
                <MovieDescript>
                    {movie.overview}
                </MovieDescript>
                {movie.genres && <GenreList>
                        { 
                            movie.genres.map(item => (<span key={item.id} >{item.name}</span>))
                        }
                </GenreList>}
            </MovieContent>
        </Link>
    </MovieContainer>
);

export default MovieItem;

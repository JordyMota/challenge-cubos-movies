import styled from 'styled-components';
// eslint-disable-next-line
import * as global from '../../assets/styles';

export const MovieListContainer = styled.ul`
    margin-top: 5vh;
    margin-bottom: 90px;
    width: 100%;
    padding: 0;
`;
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './pages/home';
import Movie from './pages/movie';

const Routes = ()=> (
    <BrowserRouter>
        <Switch>
            <Route
                exact
                path="/"
                component={Home}
            />
            <Route
                path="/movie/:id"
                component={Movie}
            />
        </Switch>
    </BrowserRouter>
);

export default Routes;
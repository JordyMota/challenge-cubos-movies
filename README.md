# Challenge Movies Cubos

Ola, seja bem vindo ao challenge movie cubos, aqui você podera pesquisar sobre seus filmes favoritos, basta seguir os passos a baixo para utilizar o app

#Utilizando

1- Clone o projeto em sua maquina local com "git clone https://gitlab.com/JordyMota/challenge-movies-cubos.git";

2- Use "cd challenge-cubos-movie" para entrar na pasta do projeto;

3- Depois use "npm i" ou "yarn" para instalar o projeto e suas dependencias;

4- Por fim use "npm start" ou "yarn start" para o rodar o web app em seu browser;

4- Agora é só pesquisar seus filmes e aproveitar! :) S2